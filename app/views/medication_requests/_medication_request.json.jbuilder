json.extract! medication_request, :id, :basic_medical_record_id, :patient_id, :user_id, :personnel_id, :status, :medication, :created_at, :updated_at
json.url medication_request_url(medication_request, format: :json)
