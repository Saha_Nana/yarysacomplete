json.extract! assigned_request, :id, :store_inventory_master_id, :store_inventory_categories_id, :quantity, :user_id, :date_assigned, :comments, :status, :store_manager_id, :created_at, :updated_at
json.url assigned_request_url(assigned_request, format: :json)
