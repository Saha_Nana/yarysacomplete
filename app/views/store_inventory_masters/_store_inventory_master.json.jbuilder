json.extract! store_inventory_master, :id, :store_inventory_categories_id, :item_name, :cost_price, :selling_price, :manufaturer_name, :user_id, :quantity, :item_location, :created_at, :updated_at
json.url store_inventory_master_url(store_inventory_master, format: :json)
