json.extract! fluid, :id, :orals, :orals_amount, :iv_fluids, :fluids_amount, :urine_amount, :emesis_amount, :drainage_amount, :user_id, :time, :patient_id, :status, :created_at, :updated_at
json.url fluid_url(fluid, format: :json)
