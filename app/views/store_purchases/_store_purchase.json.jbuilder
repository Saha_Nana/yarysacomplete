json.extract! store_purchase, :id, :store_inventory_categories_id, :item_name, :cost_price, :quantity, :date_of_purchase, :created_at, :updated_at
json.url store_purchase_url(store_purchase, format: :json)
