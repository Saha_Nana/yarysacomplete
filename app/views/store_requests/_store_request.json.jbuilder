json.extract! store_request, :id, :user_id, :store_inventory_categories_id, :store_inventory_master_id, :quantity, :comments, :status, :date_requested, :created_at, :updated_at
json.url store_request_url(store_request, format: :json)
