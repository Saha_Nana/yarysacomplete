json.extract! purchase_receipt, :id, :store_purchase_id, :image, :user_id, :status, :comments, :created_at, :updated_at
json.url purchase_receipt_url(purchase_receipt, format: :json)
