json.extract! assign_patient, :id, :patient_id, :role_id, :status, :created_at, :updated_at
json.url assign_patient_url(assign_patient, format: :json)
