json.extract! dispensary, :id, :assigned_requests_id, :status, :comments, :date_dispensed, :created_at, :updated_at
json.url dispensary_url(dispensary, format: :json)
