json.extract! admission_review_note, :id, :patient_id, :user_id, :clinic_document_id, :review_text, :status, :created_at, :updated_at
json.url admission_review_note_url(admission_review_note, format: :json)
