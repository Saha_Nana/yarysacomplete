json.extract! store_inventory_category, :id, :name, :status, :created_at, :updated_at
json.url store_inventory_category_url(store_inventory_category, format: :json)
