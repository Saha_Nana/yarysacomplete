json.extract! bill_history, :id, :service_id, :patient_id, :price, :status, :quantity, :unit_price, :created_at, :updated_at
json.url bill_history_url(bill_history, format: :json)
