json.extract! radiology_request, :id, :basic_medical_record_id, :patient_id, :user_id, :personnel_id, :status, :radiology, :created_at, :updated_at
json.url radiology_request_url(radiology_request, format: :json)
