json.extract! radio_test_result, :id, :results, :created_at, :updated_at
json.url radio_test_result_url(radio_test_result, format: :json)
