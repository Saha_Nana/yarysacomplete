json.extract! nurse_procedure, :id, :service_id, :patient_id, :status, :complaints, :created_at, :updated_at
json.url nurse_procedure_url(nurse_procedure, format: :json)
