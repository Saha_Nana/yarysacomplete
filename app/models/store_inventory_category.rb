class StoreInventoryCategory < ApplicationRecord
    validates :name, presence: true, uniqueness: true
end
