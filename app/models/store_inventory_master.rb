class StoreInventoryMaster < ApplicationRecord
    # validates :quantity, presence: true, inclusion: 1...Float::INFINITY
    # validates :store_inventory_categories_id, presence: true, inclusion: 1...Float::INFINITY
    # validates :item_name, presence: true, uniqueness: true
    # validates :cost_price, presence: true, inclusion: 0...Float::INFINITY
    # validates :selling_price, presence: true, inclusion: 0...Float::INFINITY
    # validates :user_id, presence: true, inclusion: 1...Float::INFINITY

    require 'csv'


    def self.import_data2(file,the_type)

      if the_type == "1" #normal import  

        readVal =  file.read

        # readVal = File.read(file.tempfile)

        logger.info "Reading Temp------------------------------ #{readVal}"    
      
        

        # csv = CSV.parse(readVal, headers: true, encoding: 'iso-8859-1:utf-8')

        # logger.info "RUNNIN csv--------------#{csv}------"

        #   csv.each do |row|

        #     logger.info "RUNNIN row--------------#{row}------"


        #     if row["store_inventory_categories_id"].present? && row["item_name"].present? && row["cost_price"].present? && row["selling_price"].present?  && row["manufaturer_name"].present? && row["user_id"].present? && row["quantity"].present? && row["item_location"].present? && row["temp_quantity"].present? && row["status"].present?
        #       @store_items = StoreInventoryMaster.new(store_inventory_categories_id: row["store_inventory_categories_id"], item_name: row["item_name"], cost_price: row["cost_price"], selling_price: row["selling_price"], manufaturer_name: row["manufaturer_name"], user_id: row["user_id"],  quantity: row["quantity"], 
        #       item_location: row["item_location"], temp_quantity: row["temp_quantity"],  status: row["status"])
        #       @store_items.save!

        #       logger.info "RUNNIN HEADER------------------------------"    

        #     else 
        #       logger.info "NOT HEADER------------------------------"
        #     end  
        #   end

    
          #  Thread.new do
            CSV.foreach(file.path,headers: true, encoding: 'iso-8859-1:utf-8') do |row|
              
              if row["store_inventory_categories_id"].present? && row["item_name"].present? && row["cost_price"].present? && row["selling_price"].present?  && row["manufaturer_name"].present? && row["user_id"].present? && row["quantity"].present? && row["item_location"].present? && row["temp_quantity"].present? && row["status"].present? && row["insurance_price"].present?
                      @store_items = StoreInventoryMaster.new(store_inventory_categories_id: row["store_inventory_categories_id"], item_name: row["item_name"], cost_price: row["cost_price"], selling_price: row["selling_price"], manufaturer_name: row["manufaturer_name"], user_id: row["user_id"],  quantity: row["quantity"], 
                      item_location: row["item_location"], temp_quantity: row["temp_quantity"],  status: row["status"], insurance_price: row["insurance_price"])
                      @store_items.save!

                      logger.info "RUNNIN HEADER------------------------------"    

              else 
                logger.info "NOT HEADER------------------------------"
              end  
            end
        
        end
      
      end


end
