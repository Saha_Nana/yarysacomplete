class RadioTestResult < ApplicationRecord
  #attaching files using active storage
  has_one_attached :avafile

   has_attached_file :document

  #has_one_attached :document
  #validates_attachment :document, :content_type => { :content_type => %w(application/pdf application/msword application/vnd.openxmlformats-officedocument.wordprocessingml.document) }

end
