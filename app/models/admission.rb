class Admission < ActiveRecord::Base
  
   def self.per_page
    20
  end
  
  
def self.name_search(name)
  name = "%#{name}%"
  where('surname LIKE ?', name)
end
end
