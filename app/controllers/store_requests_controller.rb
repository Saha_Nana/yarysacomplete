class StoreRequestsController < ApplicationController
  before_action :set_store_request, only: [:show, :edit, :update, :destroy]
  include ActiveModel::Conversion
  extend  ActiveModel::Naming

  # GET /store_requests
  # GET /store_requests.json
  def index
    @store_requests = StoreRequest.all
    @store_inventory_categories = StoreInventoryCategory.all

    # @store_inventory_masters = StoreInventoryMaster.new
    @store_inventory_masters = StoreInventoryMaster.all.paginate(:page=>params[:page],per_page:10)


  end

  @@one=100
  def checkout
    @store_request = StoreRequest.new
    @store_requests = StoreRequest.all
    # @store_inventory_masters = StoreInventoryMaster.paginate(:page=>params[:page],per_page:5).order('id desc')

    # roler = current_user.role_id

      logger.info ("current user role is --- #{current_user.role_id}")

      #Doctors  #Lab Tech   #Pharmacist   #Radiologist 
    if current_user.role_id == 1 || current_user.role_id == 2 || current_user.role_id == 5 || current_user.role_id == 7
      
      @store_inventory_masters = StoreInventoryMaster.where(store_inventory_categories_id: [1, 2, 3])

      #Nurses
    elsif current_user.role_id == 3
      @store_inventory_masters = StoreInventoryMaster.where(store_inventory_categories_id: [1, 2, 3, 4, 5])
  
      #Dispensary Tech    #Admin    #Receptionist   #Cashier 
    elsif current_user.role_id == 10 || current_user.role_id == 4 || current_user.role_id == 6 || current_user.role_id == 8
      @store_inventory_masters = ""
  
    else 
      @store_inventory_masters = StoreInventoryMaster.all

    end 


    #search button queries
    # @category = params[:store_inventory_master][:store_inventory_categories_id]

    # logger.info "Category selected -- #{@category}"

    @count = 0

    @@one+=2
    respond_to do |format|
    format.html
    format.js
    end

    if params[:search]
      @store_inventory_masters = StoreInventoryMaster.where(["item_name like ?","%#{params[:search]}%"]).paginate(:page=>params[:page],per_page:5)


      puts  "name found--------hjkl "
    else
      puts "not found"

    end
  end


  #Metod to Update Temp_quantity in Inventory for taking request orders quantity
  def update_temp

    checked_Id = params[:id]

    logger.info ("My id is :::: #{checked_Id}")

    quant_value = params[:quantityvalue]

    logger.info ("My quantity is :::: #{quant_value}")

    myItem = StoreInventoryMaster.update(checked_Id, :temp_quantity => quant_value)

  end 




  def update_items
    @items_list = StoreInventoryMaster.where("store_inventory_categories_id = ?", params[:store_inventory_categories_id])
    respond_to do |format|
      format.js
    end
  end


  #creating a method for posting info back to the form from a modal
  def back_to_form
    @store_items_save =[]

    # @store_inventory_masters = StoreInventoryMaster.all
    @store_request = StoreRequest.new
    @checked_value = params[:check_box][:id]
    @quantity_value = params[:store_request][:quantity] #picking quantity returned from form modal -- checkout. 
    #This is from the temp_quantity column as that is what is taken in the checkout modal using JS in form.js.erb
    @category_name = params[:store_request][:store_inventory_categories_id]


    logger.info "Quantites passed --- #{@quantity_value.inspect}"


    @checked_value.each do |values|

      @store_items = StoreInventoryMaster.find(values)

        logger.info ("---- Store Value ---- #{values.inspect}")

      
        logger.info ("--STore Item OBj --- #{@store_items.inspect}")


      @initial_quantity = @store_items.quantity
        logger.info ("quantity initially--- #{@initial_quantity}")


      @input_quantity = @store_items.temp_quantity
        logger.info ("temp quantity unassigned--- #{@input_quantity}")


      @store_items_save << @store_items

        logger.info "stor saved with temp quant value --- #{@store_items_save.inspect}"

    end


    respond_to do|format|
      format.js{render 'store_requests/form', locals: { items: @calling }}
      format.html {render 'store_requests/form', locals: { items: @calling }}
    end

  end


  def append
    @store_request = StoreRequest.new
    store_request.item_name = params[:checkbox][:id]
    if store_request.save
      render :status => 201, :json => { id: check_box.id, quantity: quantity.quantity,item_name: item_name.name }
    else
      err_msg = store_request.errors.empty?? 'Save failed' : store_request.errors.full_messages.join('; ')
      render :status => 400, :json => { message: err_msg }
    end
  end

  # GET /store_requests/1
  # GET /store_requests/1.json
  def show
  end

  # GET /store_requests/new
  def new

    @store_request = StoreRequest.new

    @store_manager_new = 1 

  end



  # POST /store_requests
  # POST /store_requests.json
  def create

    @multiple_master_id = params[:store_inventory_master_id] #picking id of item in database

    logger.info "inventory id ----- #{@multiple_master_id}"

    @store_item = StoreInventoryMaster.find(@multiple_master_id)

    logger.info "Main Inventory Item Found ---- #{@store_item.inspect}"

    
     

    logger.info "Temporary quantity ---- #{@store_item[0].temp_quantity}"

    @roler = params[:store_request][:role_id]

    logger.info "Roler ---- #{@roler}"




    #Iteration through master id to save all records. Multiple items will have a unique master_id and unique quantity.
    #The number of master id is equal to the number of quantities. Iteration through any of them( master id or quantities available) will work. 
    #My choice here is Master ID. Set value of array master id and quantity to each record
    @multiple_master_id.each do | value |

      @requested_quantity = @store_item[0].temp_quantity #Getting the updated temp_quantity value for the request

      logger.info "Temporary quantity inside loop ---- #{@requested_quantity}"


      @save_params = StoreRequest.new(user_id: params[:store_request][:user_id], store_inventory_categories_id: params[:store_request][:store_inventory_categories_id],
      comments: params[:store_request][:comments], status: params[:store_request][:status], store_manager_id: params[:store_request][:store_manager_id], 
      date_requested: params[:store_request][:date_requested], quantity: @requested_quantity , store_inventory_master_id: value, role_id: params[:store_request][:role_id])

      respond_to do |format|

        if @save_params.save!
          format.html { }
          format.json { render :show, status: :created, location: @store_request }
        else
          format.html { render :new }
          format.json { render json: @store_request.errors, status: :unprocessable_entity }
        end
      end

      logger.info "Saved Params -- #{@save_params.inspect}"

    end

    redirect_to requests_path, notice:'Item request process successful'

   
  end


 



  # GET /store_requests/1/edit
  def edit
  end



  # PATCH/PUT /store_requests/1
  # PATCH/PUT /store_requests/1.json
  def update
    respond_to do |format|
      if @store_request.update(store_request_params)
        format.html { redirect_to @store_request, notice: 'Requested item successfully updated.' }
        format.json { render :show, status: :ok, location: @store_request }
      else
        format.html { render :edit }
        format.json { render json: @store_request.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /store_requests/1
  # DELETE /store_requests/1.json
  def destroy
    @store_request.destroy
    respond_to do |format|
      format.html { redirect_to store_requests_url, notice: 'Requested Item successfully deleted.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_store_request
      @store_request = StoreRequest.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def store_request_params
      params.require(:store_request).permit(:id,:user_id, :store_inventory_categories_id, :store_inventory_master_id, :quantity, :date_assigned, :store_manager_id, :comments, :status, :date_requested, :role_id)
    end
 end