class AdmissionReviewNotesController < ApplicationController
  before_action :set_admission_review_note, only: [:show, :edit, :update, :destroy]

  # GET /admission_review_notes
  # GET /admission_review_notes.json
  def index
    @admission_review_notes = AdmissionReviewNote.all
  end

  # GET /admission_review_notes/1
  # GET /admission_review_notes/1.json
  def show
  end

  # GET /admission_review_notes/new
  def new
    @admission_review_note = AdmissionReviewNote.new
  end

  # GET /admission_review_notes/1/edit
  def edit
  end

  # POST /admission_review_notes
  # POST /admission_review_notes.json
  def create
    patient_id =  params[:admission_review_note][:patient_id] 
    
    @admission_review_note = AdmissionReviewNote.new(admission_review_note_params)

    respond_to do |format|
      if @admission_review_note.save
        format.html { redirect_to admission_vitals_path(:id => patient_id), notice: 'Admission review note was successfully created.' }
        format.json { render :show, status: :created, location: @admission_review_note }
      else
        format.html { render :new }
        format.json { render json: @admission_review_note.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admission_review_notes/1
  # PATCH/PUT /admission_review_notes/1.json
  def update
    respond_to do |format|
      if @admission_review_note.update(admission_review_note_params)
        format.html { redirect_to @admission_review_note, notice: 'Admission review note was successfully updated.' }
        format.json { render :show, status: :ok, location: @admission_review_note }
      else
        format.html { render :edit }
        format.json { render json: @admission_review_note.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admission_review_notes/1
  # DELETE /admission_review_notes/1.json
  def destroy
    @admission_review_note.destroy
    respond_to do |format|
      format.html { redirect_to admission_review_notes_url, notice: 'Admission review note was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admission_review_note
      @admission_review_note = AdmissionReviewNote.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admission_review_note_params
      params.require(:admission_review_note).permit(:patient_id, :user_id, :clinic_document_id, :review_text, :status)
    end
end
