class ClinicDocumentsController < ApplicationController
  before_action :set_clinic_document, only: [:show, :edit, :update, :destroy]
  respond_to :html, :js

  # GET /clinic_documents
  # GET /clinic_documents.json
  def index
    @edit_documents = ClinicDocument.new 
    @clinic_documents = ClinicDocument.joins("INNER JOIN patients ON patients.id = clinic_documents.patient_id
                    INNER JOIN users ON users.id = clinic_documents.user_id INNER JOIN basic_med_records ON basic_med_records.id = clinic_documents.basic_medical_record_id").select('surname,other_names,patients.mobile_number,blood_pressure,pulse,temp,respiratory_rate,fullname,occupation,address,dob,patients.user_id,basic_med_records.id,basic_med_records.complaints,symptoms,vital_signs,clinic_exams,final_diagnosis,investigations,treatment_plans').where(user_id: current_user.id,status: 1 ).order('id desc').paginate(page: params[:page], per_page: 20)

  end

  # GET /clinic_documents/1
  # GET /clinic_documents/1.json
  def show
  end

  # GET /clinic_documents/new
  def new
    @clinic_document = ClinicDocument.new
  end

  

  # GET /clinic_documents/1/edit
  def edit
  end

  # POST /clinic_documents
  # POST /clinic_documents.json
  def create

  patient_id =  params[:clinic_document][:patient_id] 
  status_params = params[:clinic_document][:status]
    
    @clinic_document = ClinicDocument.new(clinic_document_params)

    respond_to do |format|
      if @clinic_document.save
        logger.info "STATUS IS #{status_params}"
        if status_params == '1'
        format.html { redirect_to docs_path(:id => patient_id), notice: 'Clinic document was successfully created.' }
        format.json { render :show, status: :created, location: @clinic_document }
      else 
         format.html { redirect_to admission_vitals_path(:id => patient_id), notice: 'Admission document was successfully created.' }
        format.json { render :show, status: :created, location: @clinic_document }
      end
      else
        format.html { render :new }
        format.json { render json: @clinic_document.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /clinic_documents/1
  # PATCH/PUT /clinic_documents/1.json
  def update 

    respond_to do |format|
      if @clinic_document.update(clinic_document_params)
        format.html { redirect_to clinic_documents_path, notice: 'Clinical document was successfully updated.' }
        format.json { render :show, status: :ok, location: @clinic_document }
      else
        format.html { render :edit }
        format.json { render json: @clinic_document.errors, status: :unprocessable_entity }
      end
    end
  end

  def update_clinic_documents
    created_at=params[:created_at]
      logger.info "CREATED AT ID #{created_at}"

      @edit_documents1 = ClinicDocument.find_by(created_at: created_at)
        @id = @edit_documents1.id
        logger.info "C-------------ID #{@id}-------------"

    complaints = params[:clinic_document][:complaints]
     history_complaints = params[:clinic_document][:history_complaints]
     direct_question = params[:clinic_document][:direct_question]
     systems_review = params[:clinic_document][:systems_review]
    med_history = params[:clinic_document][:med_history]
    social_history = params[:clinic_document][:social_history]
    allergies = params[:clinic_document][:allergies]
    symptoms = params[:clinic_document][:symptoms]
    
    vital_signs = params[:clinic_document][:vital_signs]
    clinic_exams = params[:clinic_document][:clinic_exams]
    final_diagnosis = params[:clinic_document][:final_diagnosis]
    investigations = params[:clinic_document][:investigations]
    treatment_plans = params[:clinic_document][:treatment_plans]

    patient_id = params[:clinic_document][:patient_id]
  

   @clinic_document = ClinicDocument.update( @id,{:complaints => complaints,:history_complaints => history_complaints,:direct_question => direct_question,:systems_review => systems_review,:med_history => med_history, :social_history => social_history ,:allergies=> allergies, :symptoms => symptoms,:vital_signs=> vital_signs, :clinic_exams => clinic_exams,
    :final_diagnosis => final_diagnosis, :investigations => investigations, :treatment_plans => treatment_plans})
    
    #@clinic_document = ClinicDocument.update( @id,{:complaints => complaints,:med_history => med_history})
    #@clinic_document.update(clinic_document_params)

    respond_to do |format|
    format.html { redirect_to  docs_path(:id => patient_id), notice: 'Clinical document was successfully updated.' }
    end
    #    
   # puts AssignPatient.update(@assign_patient_id, :status => 1)

    # respond_to do |format|
    #   if @clinic_document.update(clinic_document_params)
    #     format.html { redirect_to clinic_documents_path, notice: 'Clinical document was successfully updated.' }
    #     format.json { render :show, status: :ok, location: @clinic_document }
    #   else
    #     format.html { render :edit }
    #     format.json { render json: @clinic_document.errors, status: :unprocessable_entity }
    #   end
    #end
  end

  # DELETE /clinic_documents/1
  # DELETE /clinic_documents/1.json
  def destroy
    @clinic_document.destroy
    respond_to do |format|
      format.html { redirect_to clinic_documents_url, notice: 'Clinic document was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_clinic_document
      @clinic_document = ClinicDocument.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def clinic_document_params
      params.require(:clinic_document).permit(:med_history,:social_history,:allergies,:patient_id, :basic_medical_record_id, :user_id, :status, :complaints, :symptoms, :vital_signs, :clinic_exams, :final_diagnosis, :investigations, :treatment_plans,:history_complaints,:direct_question,:systems_review)
    end
end
