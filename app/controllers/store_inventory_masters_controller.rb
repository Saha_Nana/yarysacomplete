class StoreInventoryMastersController < ApplicationController
  before_action :set_store_inventory_master, only: [:show, :edit, :update, :destroy]

  # GET /store_inventory_masters
  # GET /store_inventory_masters.json
  def index

     if params[:count]
      params[:count]
    else
      params[:count] = 100 
    end

    if params[:page]
      page = params[:page].to_i
    else
    page = 1
    end

    if params[:per_page].present?
      perpage = params[:per_page]
    else
    perpage = 100
    end

    @per_page = params[:per_page] || Patient.per_page || 100

    page = if params[:page]
      params[:page].to_i
    else
    1
    end

    per_page = 100
    offset = (page - 1) * per_page
    limit = page * per_page
    @array = *(offset...limit)


    #  @count = StoreInventoryMaster.where(:quantity).count
    logger.info "@med_count---#{@med_count}"

    @import = StoreInventoryMaster.new

    
    @store_inventory_masters = StoreInventoryMaster.joins("INNER JOIN store_inventory_categories on store_inventory_categories.id = store_inventory_masters.store_inventory_categories_id")
      .select("store_inventory_masters.id", "store_inventory_masters.store_inventory_categories_id as category_id", "name", "item_name", "selling_price", "cost_price", "manufaturer_name", 
      "user_id", "item_location", "insurance_price","quantity", "date_of_purchase", "store_inventory_masters.status as masters_status","store_inventory_categories.status as category_status", "temp_quantity", "image" )
      .paginate(:page => params[:page], :per_page => params[:count]).order('id desc')
  end

  # GET /store_inventory_masters/1
  # GET /store_inventory_masters/1.json
  def show
  end

  # GET /store_inventory_masters/new
  def new
    @store_inventory_master = StoreInventoryMaster.new
  end

  # GET /store_inventory_masters/1/edit
  def edit
  end

  # POST /store_inventory_masters
  # POST /store_inventory_masters.json
  def create
    @store_inventory_master = StoreInventoryMaster.new(store_inventory_master_params)

    respond_to do |format|
      if @store_inventory_master.save
        format.html { redirect_to store_inventory_masters_path, notice: 'New inventory item added successfully.' }
        format.json { render :show, status: :created, location: @store_inventory_master }
      else
        format.html { render :new }
        format.json { render json: @store_inventory_master.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /store_inventory_masters/1
  # PATCH/PUT /store_inventory_masters/1.json
  def update
    respond_to do |format|
      if @store_inventory_master.update(store_inventory_master_params)
        format.html { redirect_to root_path, notice: 'Inventory item was successfully updated.' }
        format.json { render :show, status: :ok, location: @store_inventory_master }
      else
        format.html { render :edit }
        format.json { render json: @store_inventory_master.errors, status: :unprocessable_entity }
      end
    end
  end


  #Importing CSv Files
  def import_csv
    @import = StoreInventoryMaster.new

    respond_to do |format|
      
        format.js
    end

  end

  def import_file

    @import = StoreInventoryMaster.new
   
      if params[:file].nil? 
        redirect_to store_inventory_masters_path, notice: "Kindly select A file to upload"
      else               
        # check = ""        
        check = StoreInventoryMaster.import_data2(params[:file],"1")        
        logger.info check.inspect
        redirect_to store_inventory_masters_path, notice: "Data successfully imported."
      end

  end

  # DELETE /store_inventory_masters/1
  # DELETE /store_inventory_masters/1.json
  def destroy
    @store_inventory_master.destroy
    respond_to do |format|
      format.html { redirect_to store_inventory_masters_url, notice: 'Inventory item was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_store_inventory_master
      @store_inventory_master = StoreInventoryMaster.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def store_inventory_master_params
      params.require(:store_inventory_master).permit(:id,:store_inventory_categories_id,:temp_quantity, :status, :item_name, :cost_price, :selling_price, :manufaturer_name, :user_id, :quantity, :item_location, :insurance_price)
    end
end
