class AssignPatientsController < ApplicationController
  before_action :set_assign_patient, only: [:show, :edit, :update, :destroy]
  
  #  require './lib/push/notify.rb' 
  layout "doc_patients",:only => [ :docs]


  def index
    @edit_documents = ClinicDocument.new
    @vital_record = BasicMedRecord.new
     @nurse_procedure = NurseProcedure.new
    #@assign_patients = AssignPatient.joins(:patients).select('surname,other_names,mobile_number,occupation,address,dob,patients.user_id').group('patient_id').where(nurse_id: current_user.id )
    @clinic_document = ClinicDocument.new

    @assign_patients= AssignPatient.joins("INNER JOIN patients ON patients.id = assign_patients.patient_id
                  INNER JOIN users ON users.id  = assign_patients.user_id  INNER JOIN services_inventories ON services_inventories.id  = assign_patients.service_id").select('service_name,surname,
                  other_names,patients.mobile_number,fullname,occupation, address, dob,patients.user_id,assign_patients.patient_id,assign_patients.id,assign_patients.status,basic_medical_record_id,
                  personnel_id,users.role_id,card_no,assign_patients.created_at').where(personnel_id: current_user.role_id, status: 1).order('id desc').paginate(page: params[:page], per_page: 20)

     @assign_patients_to_doctors= AssignPatient.joins("INNER JOIN patients ON patients.id = assign_patients.patient_id
                  INNER JOIN users ON users.id  = assign_patients.user_id ").select('surname,other_names,patients.mobile_number,fullname,occupation,address,dob,patients.user_id,assign_patients.patient_id,assign_patients.id,assign_patients.status,basic_medical_record_id,personnel_id,users.role_id,card_no,assign_patients.created_at').where(personnel_id: current_user.id,status: 1).order('id desc').paginate(page: params[:page], per_page: 20)

    @assign_to_doctor = AssignPatient.new
    @assign_patient = AssignPatient.new
    @lab_request = LabRequest.new
    @medication_request = MedicationRequest.new
    
    
    if params[:count]
      params[:count]
    else
      params[:count] = 20 
    end

    if params[:page]
      page = params[:page].to_i
    else
    page = 1
    end

    if params[:per_page].present?
      perpage = params[:per_page]
    else
    perpage = 20
    end

    @per_page = params[:per_page] || AssignPatient.per_page || 20

    page = if params[:page]
      params[:page].to_i
    else
    1
    end

    per_page = 20
    offset = (page - 1) * per_page
    limit = page * per_page
    @array = *(offset...limit)

    if params[:search_value] && params[:search_value].strip != ''
      if params[:search_param] == 'card_no'
        @assign_patients = AssignPatient.patient_no_search(params[:search_value].strip).paginate(page: params[:page],per_page: params[:count]).order('created_at asc')

        @assign_patients_to_doctors = AssignPatient.patient_no_search_doc(params[:search_value].strip).paginate(page: params[:page],per_page: params[:count]).order('created_at asc')

      elsif params[:search_param] == 'name'
        @assign_patients = AssignPatient.name_search(params[:search_value].strip).paginate(page: params[:page],per_page: params[:count]).order('created_at asc')

              @assign_patients_to_doctors = AssignPatient.name_search_doc(params[:search_value].strip).paginate(page: params[:page],per_page: params[:count]).order('created_at asc')




      elsif params[:search_param] == 'f_name'
        @assign_patients = AssignPatient.f_name_search(params[:search_value].strip).paginate(page: params[:page],per_page: params[:count]).order('created_at asc')

          @assign_patients_to_doctors = AssignPatient.f_name_search_doc(params[:search_value].strip).paginate(page: params[:page],per_page: params[:count]).order('created_at asc')
      end
    elsif params[:search_param] == 'date'
      start = (params["start_date"] + " " + "0:00:00")# Time.zone.parse(params["start_date"].to_s + " " + "0:00:00").utc # params["start_date"].to_s + "0:00:00"
      ended = params["end_date"] + " " + ("23:59:59") # Time.zone.parse(params["end_date"].to_s + " " + "23:59:59").utc # params["end_date"].to_s + "23:59:59"

      @assign_patients = AssignPatient.date_search(start,ended).paginate(page: params[:page], per_page: params[:count]).order('created_at asc')

    else
    end

  end

  # GET /assign_patients/1
  # GET /assign_patients/1.json
  def show
  end

  # GET /assign_patients/new
  def new
    @assign_patient = AssignPatient.new

    @patient = Patient.find(params[:id])

    session[:return_to] ||= request.referer
    @record = Patient.find_by(id: patient_id)

    logger.info "Lets see the records"
    logger.info "record: #{@record.inspect}"
    logger.info "patient id: #{@record.id}"
    p "record: #{@record.inspect}"
    logger.info "---------------------------------------------"
    logger.info "---------------------------------------------"
    @patient = Patient.new
  end

  # GET /assign_patients/1/edit
  def edit
  end

  def show_details
    patient_id=params[:id]

    @patients1 = Patient.find_by(id: patient_id)
    logger.info "patients1: #{@patients1}"
    @patients1.other_names
    logger.info "other_names: #{@patients1.other_names}"
  end

  def basic_medical
    @vital_record = BasicMedRecord.new
    @created_at=params[:id]

    logger.info "Lets see id para #{@created_at}"
    patient_record = AssignPatient.find_by(created_at: @created_at)
    @patient_id =  patient_record.patient_id
    logger.info "Lets see the patient id in basic medical is #{@patient_id}"

    @assign_patient = AssignPatient.new

    respond_to do |format|

      format.html
      format.js
    end
  end



   def procedure_list
     @nurse_procedure = NurseProcedure.new
    @created_at=params[:id]

    logger.info "Lets see id para #{@created_at}"
    patient_record = AssignPatient.find_by(created_at: @created_at)
    @patient_id =  patient_record.patient_id
    logger.info "Lets see the patient id inPROCEDURE  is #{@patient_id}"




    @assign_patient = AssignPatient.new

    #@services_inventories = ServicesInventory.order(:service_name).where("service_name like ? AND (category_id != 1 AND category_id != 3)", "%#{params[:service_id]}" )
    @services_inventories = ServicesInventory.where("category_id = 8  ")
    @service_list = @services_inventories.map { |a|[a.service_name+" ",a.id]  }

    respond_to do |format|

      format.html
      format.js
    end
  end


   #Metod to Update Temp_quantity in Inventory for taking request orders quantity
  def update_qtytemp

    @nurse_procedure = NurseProcedure.new  
   

    checked_Id = params[:id]

    logger.info ("My id is :::: #{checked_Id}")

    quant_value = params[:quantityvalue]

    logger.info ("My quantity is :::: #{quant_value}")

    myItem = ServicesInventory.update(checked_Id, :temp_quantity => quant_value)

  end 
  
  def basic_vitals
   @vital_record = BasicMedRecord.new
 
   patient_id  = params[:basic_med_record][:patient_id]
   blood_pressure  = params[:basic_med_record][:blood_pressure]
   pulse  = params[:basic_med_record][:pulse]
   temp  = params[:basic_med_record][:temp]
   respiratory_rate  = params[:basic_med_record][:respiratory_rate]
   height  = params[:basic_med_record][:height]
   weight  = params[:basic_med_record][:weight]
   saturation  = params[:basic_med_record][:saturation]
   user_id  = params[:basic_med_record][:user_id]
   status  = params[:basic_med_record][:status]
   complaints  = params[:basic_med_record][:complaints]
   admission_status  = params[:basic_med_record][:admission_status]
   @info = BasicMedRecord.new({:patient_id => patient_id,:blood_pressure => blood_pressure,:pulse => pulse,:temp => temp,:respiratory_rate => respiratory_rate,:height=> height,:weight=> weight,:saturation=> saturation,:user_id => user_id,:status => status,:complaints => complaints, :admission_status => admission_status})
   @info.save
   
   respond_to do |format|
      
        format.html { redirect_to basic_med_records_path, notice: 'Patient Vitals Taken Successfully ' }
        format.js
    end
    
  end
  
  def clinic_documentation

    @assign_patient = AssignPatient.new
    @clinic_document = ClinicDocument.new

    patient_id=params[:id]
    @patient_id = patient_id


    @b_records = BasicMedRecord.where(patient_id: patient_id).order('id desc')
     
    @blood_pressure = @b_records[0].blood_pressure
    @pulse = @b_records[0].pulse
    @respiratory = @b_records[0].respiratory_rate
    @temperature =@b_records[0].temp
    @patient_id = @b_records[0].patient_id
    @height  = @b_records[0].height
    @weight  = @b_records[0].weight
    @saturation  = @b_records[0].saturation


    #@clinic_records = ClinicDocument.find_by(patient_id: patient_id).order('id desc')
    
    @clinic_records = ClinicDocument.where(patient_id: patient_id).order('id desc')
     if @clinic_records.exists?
     @complaints = @clinic_records[0].complaints
     @history_complaints = @clinic_records[0].history_complaints
     @direct_question = @clinic_records[0].direct_question
     @systems_review = @clinic_records[0].systems_review
     @symptoms = @clinic_records[0].symptoms
     
     @med_history = @clinic_records[0].med_history
     @social_history = @clinic_records[0].social_history
     @allergies = @clinic_records[0].allergies
     @clinic_exams  = @clinic_records[0].clinic_exams
       @final_diagnosis  = @clinic_records[0].final_diagnosis
         @investigations  = @clinic_records[0].investigations
         @treatment_plans  = @clinic_records[0].treatment_plans
      logger.info "Lets see clinic_records #{@clinic_records.inspect}"  
      logger.info "Lets see complaints #{@complaints}"
     else
     end
     
     

    @vital_signs = "Respiratory Rate: " + @b_records[0].respiratory_rate + " " + "Pulse: " +  @b_records[0].pulse + " " + "Blood Pressure: " +  @b_records[0].blood_pressure + " " + "Temperature: " +  @b_records[0].temp + " " + "Oxygen saturation: " +  @b_records[0].saturation.to_s + " " + "Weight: " +  @b_records[0].weight.to_s + " " + "Height: " +  @b_records[0].height.to_s

    logger.info "Vital signs: #{@vital_signs}"

    if (defined?(@b_records)).nil?
      @ass_p_records = AssignPatient.find_by(patient_id: patient_id)

      AssignPatient.update(@ass_p_records.id, :status => 1)

      logger.info "------------------------------------------"
      logger.info "Lets see the b records"
      logger.info "basic medical record: #{@b_records.inspect}"
      logger.info "------------------------------------------"

      @blood_pressure = @b_records.blood_pressure
      @pulse = @b_records.pulse
      @respiratory = @b_records.respiratory_rate
      @temperature = @b_records.temp
      @patient_id = @b_records.patient_id

      patient_id = @patient_id

      @basic_med_id = @b_records.id
      basic_med_id = @basic_med_id
      logger.info "patient id: #{@patient_id}"
      logger.info " basic_med_id: #{@basic_med_id}"
      logger.info "   @b_records.blood_pressure: #{ @blood_pressure}"

    else

      logger.info " LIEEEEESSSSS"

    end

    respond_to do |format|

      format.js
      format.html
    end
  end


   def edit_docs


    @edit_documents = ClinicDocument.new
    @clinic_document = ClinicDocument.new
    @created_at=params[:id]
    logger.info "created_at edit docs: #{@created_at}"
    @edit_documents1 = ClinicDocument.find_by(created_at: @created_at)
   logger.info "edit_documents: #{@edit_documents1.inspect}"
   logger.info "complaints: #{@edit_documents1.complaints}"
   @complaints = @edit_documents1.complaints
    @history_complaints = @edit_documents1.history_complaints
     @direct_question = @edit_documents1.direct_question
     @systems_review = @edit_documents1.systems_review
   @med_history = @edit_documents1.med_history
   @social_history = @edit_documents1.social_history
   @allergies = @edit_documents1.allergies
   @symptoms = @edit_documents1.symptoms
   @vital_signs = @edit_documents1.vital_signs
   @clinic_exams = @edit_documents1.clinic_exams
   @final_diagnosis = @edit_documents1.final_diagnosis
   @investigations = @edit_documents1.investigations
   @treatment_plans = @edit_documents1.treatment_plans
   @patient_id = @edit_documents1.patient_id
   @symptoms = @edit_documents1.symptoms



    respond_to do |format|

      format.html
      format.js
    end

  end

  def view_documentation

    @assign_patient = AssignPatient.new
    created_at=params[:id]

    @documents = ClinicDocument.where(created_at: created_at)
    logger.info "document1: #{@documents.inspect}"

    respond_to do |format|

      format.html
      format.js
    end

  end

  def assign_lab
    @assign_patient = AssignPatient.new
    @lab_request = LabRequest.new
    basic_med_id=params[:id]

    @b_records = BasicMedRecord.find_by(id: basic_med_id)

    @patient_id = @b_records.patient_id
    patient_id = @patient_id

    @basic_med_id = @b_records.id
    basic_med_id = @basic_med_id

    respond_to do |format|

      format.html
      format.js
    end

  end

  def assign_med
    @assign_patient = AssignPatient.new
    @medication_request = MedicationRequest.new
    basic_med_id=params[:id]

    @b_records = BasicMedRecord.find_by(id: basic_med_id)

    @patient_id = @b_records.patient_id
    patient_id = @patient_id

    @basic_med_id = @b_records.id
    basic_med_id = @basic_med_id

    respond_to do |format|

      format.html
      format.js
    end
  end

  def docs

    
    @edit_documents = ClinicDocument.new
    @assign_patient = AssignPatient.new
    @clinic_document = ClinicDocument.new

    patient_id =params[:id]
    @patient_id = patient_id

    @patient_records = Patient.find_by(id: patient_id)


    retrieve_dob = @patient_records.dob
    now = Time.now.utc.to_date

    logger.info "now IS #{now.year} dob #{retrieve_dob.year}"
   # now.year - retrieve_dob.year - ((now.month > retrieve_dob.month || (now.month == retrieve_dob.month && now.day >= retrieve_dob.day)) ? 0 : 1)
    @age = now.year - retrieve_dob.year
    logger.info "AGE IS #{@age}"


    logger.info "Lets see patient_id id #{@patient_id}"
    logger.info "Lets see  Patient name #{@patient_records.surname } #{@patient_records.other_names }"
    if (defined?(@patient_id)).nil?
      logger.info "XXXXXXXXXXXXXXXXXXXXXX"

      @clinic_documents = ClinicDocument.joins("INNER JOIN patients ON patients.id = clinic_documents.patient_id
                    INNER JOIN users ON users.id = clinic_documents.user_id").select('surname,other_names,patients.mobile_number,fullname,occupation,address,dob,patients.user_id,complaints,symptoms,vital_signs,clinic_exams,final_diagnosis,investigations,treatment_plans,card_no,clinic_documents.created_at,clinic_documents.patient_id,clinic_documents.id').where(patient_id: patient_id ,status: 1,created_at: Time.zone.now.beginning_of_day..Time.zone.now.end_of_day).order('id desc').paginate(page: params[:page], per_page: 5)


     

    else

       @b_records = BasicMedRecord.where(patient_id: @patient_id).order('id desc')
       #BasicMedRecord.find_by(patient_id:  @patient_id)
       logger.info "Lets see BasicMedRecord id #{@b_records.inspect}"
      @blood_pressure = @b_records[0].blood_pressure 
      @pulse = @b_records[0].pulse
      @respiratory = @b_records[0].respiratory_rate
      @temperature = @b_records[0].temp
       @height  = @b_records[0].height
      @weight  = @b_records[0].weight
      @saturation  = @b_records[0].saturation
      @patient_id = @b_records[0].patient_id

      patient_id = @patient_id

      @basic_med_id = @b_records[0].id
      basic_med_id = @basic_med_id

      logger.info "Lets see medical id #{patient_id}"
      
     @clinic_documents = ClinicDocument.joins("INNER JOIN patients ON patients.id = clinic_documents.patient_id
                    INNER JOIN users ON users.id = clinic_documents.user_id").select('surname,other_names,patients.mobile_number,fullname,occupation,address,dob,patients.user_id,complaints,symptoms,vital_signs,clinic_exams,final_diagnosis,investigations,treatment_plans,card_no,clinic_documents.created_at,clinic_documents.patient_id,clinic_documents.id').where(patient_id: patient_id ,status: 1).order('id desc').paginate(page: params[:page], per_page: 5 )


      logger.info "CLINIC DOCUMENTS: #{@clinic_documents.inspect}"

      @document1 = ClinicDocument.find_by(patient_id: patient_id)
 
    end

    respond_to do |format|

      format.html
      format.js
    end

  end


 
  def assign_to_a_doctor
    logger.info "---------------------------ASSIGED DOCTOR---------------------------------"

    personnel_id = params[:assign_patient][:personnel_id]
    patient_id = params[:assign_patient][:patient_id]
    role_id = params[:assign_patient][:role_id]
    user_id = params[:assign_patient][:user_id]
    status = params[:assign_patient][:status]
    basic_medical_record_id = params[:assign_patient][:basic_medical_record_id]
    service_id = 4 
  
    @assign_to_doctor = AssignPatient.new({:patient_id => patient_id,:role_id => role_id, :status => status ,:personnel_id=> personnel_id,:user_id=> user_id,:service_id=> service_id})


    respond_to do |format|
      if @assign_to_doctor.save

        format.html { redirect_to basic_med_records_path, notice: 'Patient was successfully assigned to a Doctor.' }
        format.json { render :show, status: :created, location: @assign_patient }

        #NOTIFICATION TO DOCTORS
      #  notification_for_user = '92ba9439-06a5-497d-93e8-18bb36fb2a73'
      #  logger.info "NOTIFCATIONS --------"
      #   push =  Push::Notify.single_notification(notification_for_user,"Patient Assigned To you")
      #   logger.info push
      #   logger.info "ENDS--------"

      else
        format.html { render :new }
        format.json { render json: @assign_patient.errors, status: :unprocessable_entity }
      end


       
    end
  end

  
  
  #NEW METHOD VITALS
  def view_vitals
     id = params[:id]

    @documents = BasicMedRecord.where(patient_id: id).order('id desc')
    logger.info "document1: #{@documents.inspect}"

    @vital_signs = "Respiratory Rate " + @documents[0].respiratory_rate + " " + "Pulse " +  @documents[0].pulse + " " + "blood pressure " +  @documents[0].blood_pressure + " " + "Temperature" +  @documents[0].temp

    logger.info "Vital signs: #{@vital_signs}"

    respond_to do |format|

      format.html
      format.js
    end
  end
  
  # POST /assign_patients
  # POST /assign_patients.json
  def create
     personnel_id = params[:assign_patient][:personnel_id]
    patient_id = params[:assign_patient][:patient_id]
    service_id = params[:assign_patient][:service_id]
    role_id = params[:assign_patient][:role_id]
    user_id = params[:assign_patient][:user_id]
    status = params[:assign_patient][:status]

   @p_id_ankasa = Patient.find_by(id: patient_id)
   @p_id = @p_id_ankasa.id
   puts "PATIENS ID IN CREARE #{@p_id}"

   puts "CONSULTATION ID  #{service_id}"

   

   if   personnel_id == '3'  #NURSE ID

    
      @services_data = ServicesInventory.find_by(id: service_id)
      price = @services_data.price  #Price for consultation depends on the service id

      if BillTemp.where(patient_id: patient_id).exists?
        @id = BillTemp.find_by(patient_id: patient_id)
        @bills = BillHistory.new({:service_id => service_id,:patient_id => patient_id, :price => price ,:status=> 0,:delete_status => 0})
        @bills_temp = BillTemp.update(@id.id, :created_at => Time.now )
      @bills.save

      else
        @bills = BillHistory.new({:service_id => service_id,:patient_id => patient_id, :price => price ,:status=> 0,:delete_status => 0})
        @bills_temp = BillTemp.new({:patient_id => patient_id, :status=> 0})
      @bills.save
      @bills_temp.save
      end

      @assign_patient = AssignPatient.new({:patient_id => patient_id,:role_id => role_id, :status => status ,:personnel_id=> personnel_id,:user_id=> user_id,:service_id=> service_id})
        Patient.update(@p_id, :assigned_status => 1 )
       Patient.update(@p_id, :updated_at => Time.now)
   
    else
      @bills_temp = BillTemp.new({:patient_id => patient_id, :status=> 0})
    @bills_temp.save

    end




   

    respond_to do |format|
      if @assign_patient.save 
        if personnel_id == '1'
          # flash[:success] = "Assign patient was successfully created"
          format.html { redirect_to patients_path, notice: 'Patient was successfully assigned to a Doctor.' }
          format.json { render :show, status: :created, location: @assign_patient }

        elsif personnel_id == '2'
          # flash[:success] = "Assign patient was successfully created"
          format.html { redirect_to patients_path, notice: 'Patient was successfully assigned to a Lab technician.' }
          format.json { render :show, status: :created, location: @assign_patient }

        elsif personnel_id == '3'
          
          format.html { redirect_to patients_path, notice: 'Patient was successfully assigned to a nurse.' }
          format.json { render :show, status: :created, location: @assign_patient }

          #NOTIFICATION TO NURSES
          # notification_for_user = '92ba9439-06a5-497d-93e8-18bb36fb2a73'
          # logger.info "NOTIFCATIONS --------"
          #  push =  Push::Notify.single_notification(notification_for_user,"Patient Assigned To you for Vitals")
          #  logger.info push
          #  logger.info "ENDS--------"




    #NOTIFICATION TO CASHIER
    # notification_for_user = '92ba9439-06a5-497d-93e8-18bb36fb2a73'
    # logger.info "NOTIFCATIONS --------"
    #  push =  Push::Notify.single_notification(notification_for_user,"NEW BILL GENERATED FOR NURSE VITALS")
    #  logger.info push
    #  logger.info "ENDS--------"

        elsif personnel_id == '5'
          # flash[:success] = "Assign patient was successfully created"
          format.html { redirect_to patients_path, notice: 'Patient was successfully assigned to a pharmacist.' }
          format.json { render :show, status: :created, location: @assign_patient }

        else

          format.html { redirect_to patients_path, notice: 'Patient was successfully nurse only.' }
          format.json { render :show, status: :created, location: @assign_patient }

        end
      else
        format.html { render :new }
        format.json { render json: @assign_patient.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /assign_patients/1
  # PATCH/PUT /assign_patients/1.json
  def update
    respond_to do |format|
      if @assign_patient.update(assign_patient_params)
        format.html { redirect_to @assign_patient, notice: 'Assign patient was successfully updated.' }
        format.json { render :show, status: :ok, location: @assign_patient }
      else
        format.html { render :edit }
        format.json { render json: @assign_patient.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /assign_patients/1
  # DELETE /assign_patients/1.json
  def destroy
    @assign_patient.destroy
    respond_to do |format|
      format.html { redirect_to assign_patients_url, notice: 'Assign patient was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_assign_patient
    @assign_patient = AssignPatient.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def assign_patient_params
    params.require(:assign_patient).permit(:service_id,:patient_id, :role_id, :status,:personnel_id,:user_id, :basic_medical_record_id)
  end
end
