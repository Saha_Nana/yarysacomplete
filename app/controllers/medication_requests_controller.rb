class MedicationRequestsController < ApplicationController
  before_action :set_medication_request, only: [:show, :edit, :update, :destroy]

   layout "doc_patients",:only => [ :med_docs]

  # GET /medication_requests
  # GET /medication_requests.json
  def index
    

     @medication_requests = MedicationRequest.joins("INNER JOIN patients ON patients.id = medication_requests.patient_id
                    INNER JOIN users ON users.id = medication_requests.user_id INNER JOIN basic_med_records ON basic_med_records.id = medication_requests.basic_medical_record_id").select('surname,other_names,patients.mobile_number,card_no,blood_pressure,pulse,temp,respiratory_rate,fullname,occupation,address,dob,patients.user_id,basic_med_records.id,medication,medication_requests.created_at,medication_requests.patient_id,personnel_id').where(personnel_id: current_user.role_id ).paginate(page: params[:page], per_page: 20)

      @assign_patients= AssignPatient.joins("INNER JOIN patients ON patients.id = assign_patients.patient_id
                  INNER JOIN users ON users.id = assign_patients.user_id").select('surname,other_names,patients.mobile_number,fullname,occupation,address,dob,patients.user_id,patient_id,assign_patients.id,assign_patients.status,basic_medical_record_id,personnel_id,users.role_id,card_no').where(personnel_id: current_user.role_id).order('id desc').paginate(page: params[:page], per_page: 20)

  end


  def other_med
    
      @medication_requests = OrderRequest.joins("INNER JOIN patients ON patients.id = order_requests.patient_id
                    INNER JOIN users ON users.id = order_requests.user_id INNER JOIN services_inventories ON services_inventories.id = order_requests.service_id").select('services_inventories.service_name,services_inventories.category_id,surname,other_names,patients.mobile_number,card_no,fullname,occupation,address,dob,patients.user_id,order_requests.created_at,order_requests.patient_id,personnel_id,order_requests.service_id,order_requests.status,order_requests.id').where('services_inventories.category_id = 2').order('order_requests.id desc')
      
  end


   def close_request
    med_id =params[:id]
    logger.info "Lets see medical id #{med_id}"
    if OrderRequest.update(med_id, :status => 1)
      redirect_to other_med_path, notice: 'Request Closed.'
    end
  end

  def open_request
    med_id =params[:id]
    if OrderRequest.update(med_id, :status => 0)
      redirect_to other_med_path, notice: 'Request Open' 
    end
  end

2
  # GET /medication_requests/1
  # GET /medication_requests/1.json
  def show
  end

  # GET /medication_requests/new
  def new
    @medication_request = MedicationRequest.new
  end

  # GET /medication_requests/1/edit
  def edit
  end

  # POST /medication_requests
  # POST /medication_requests.json
  def create

     service_id = params[:medication_request][:service_id] 
 patient_id = params[:medication_request][:patient_id] 
@services_data = ServicesInventory.find_by(id: service_id)
price = @services_data.price
      logger.info "--------------------------------------------"
      logger.info "-------------service_id#{service_id}-------------------------------"
      logger.info "-------------patient_id#{patient_id}-------------------------------"
      logger.info "-------------price#{price}-------------------------------"
      logger.info "--------------------------------------------"
 @bills = BillHistory.new({:service_id => service_id,:patient_id => patient_id, :price => price ,:status=> 0,:delete_status => 0})
 
 @bills.save
 

    @medication_request = MedicationRequest.new(medication_request_params)


    respond_to do |format|
      if @medication_request.save
        format.html { redirect_to med_docs_path(:id => patient_id), notice: 'Medication request was successfully created.' }
        format.json { render :show, status: :created, location: @medication_request }



         #NOTIFICATION TO PHARMACIST
        #  notification_for_user = '92ba9439-06a5-497d-93e8-18bb36fb2a73'
        #  logger.info "NOTIFCATIONS --------"
        #   push =  Push::Notify.single_notification(notification_for_user,"NEW MEDICATION REQUEST FROM DOC - CHECK")
        #   logger.info push
        #   logger.info "ENDS--------"


    #NOTIFICATION TO CASHIER
    # notification_for_user = '92ba9439-06a5-497d-93e8-18bb36fb2a73'
    # logger.info "NOTIFCATIONS --------"
    #  push =  Push::Notify.single_notification(notification_for_user,"NEW BILL GENERATED FOR MEDICATIONS")
    #  logger.info push
    #  logger.info "ENDS--------"
      else
        format.html { render :new }
        format.json { render json: @medication_request.errors, status: :unprocessable_entity }
      end
    end
  end




   def med_docs

     @assign_patient = AssignPatient.new
      @clinic_document = ClinicDocument.new
      @medication_request = MedicationRequest.new

                patient_id =params[:id] 
  @patient_id = patient_id

    @patient_records = Patient.find_by(id: patient_id)
     

    logger.info "Lets see patient_id id #{@patient_id}" 
    logger.info "Lets see  Patient name #{@patient_records.surname } #{@patient_records.other_names }" 

        if (defined?(patient_id)).nil? 

     @medication_requests = MedicationRequest.joins("INNER JOIN patients ON patients.id = medication_requests.patient_id
                    INNER JOIN users ON users.id = medication_requests.user_id INNER JOIN basic_med_records ON basic_med_records.id = medication_requests.basic_medical_record_id").select('surname,other_names,patients.mobile_number,card_no,blood_pressure,pulse,temp,respiratory_rate,fullname,occupation,address,dob,patients.user_id,basic_med_records.id,medication,card_no,medication_requests.created_at,medication_requests.patient_id').where(patient_id: patient_id )
    

      @b_records = BasicMedRecord.find_by(patient_id: patient_id)
       @blood_pressure = @b_records.blood_pressure
     @pulse = @b_records.pulse
     @respiratory = @b_records.respiratory_rate
     @temperature = @b_records.temp
     @patient_id = @b_records.patient_id
  
     patient_id = @patient_id

      @basic_med_id = @b_records.id
     basic_med_id = @basic_med_id
   
    logger.info "Lets see medical id #{patient_id}"

    @clinic_documents = ClinicDocument.joins("INNER JOIN patients ON patients.id = clinic_documents.patient_id
                    INNER JOIN users ON users.id = clinic_documents.user_id INNER JOIN basic_med_records ON basic_med_records.id = clinic_documents.basic_medical_record_id").select('surname,other_names,patients.mobile_number,blood_pressure,pulse,temp,respiratory_rate,fullname,occupation,address,dob,patients.user_id,basic_med_records.id,complaints,symptoms,vital_signs,clinic_exams,final_diagnosis,investigations,treatment_plans,card_no,clinic_documents.created_at,clinic_documents.patient_id').where(patient_id: patient_id )
   

     logger.info "CLINIC DOCUMENTS: #{@clinic_documents}"
  
     @document1 = ClinicDocument.find_by(patient_id: patient_id)
      logger.info "document1: #{@document1}"
       logger.info "complaints: #{@document1.complaints}"

     else
       @medication_requests = MedicationRequest.joins("INNER JOIN patients ON patients.id = medication_requests.patient_id
                    INNER JOIN users ON users.id = medication_requests.user_id ").select('surname,other_names,patients.mobile_number,card_no,fullname,occupation,address,dob,patients.user_id,medication,card_no,medication_requests.created_at,medication_requests.patient_id').where(patient_id: patient_id)
     end

    

    respond_to do |format|

        format.html
        format.js
  end

  end



    def med_form
    @assign_patient = AssignPatient.new
   @medication_request = MedicationRequest.new


     patient_id=params[:id] 
     @patient_id = patient_id
   
    logger.info "Lets see medical id #{@patient_id}"


 @services_inventories = ServicesInventory.order(:service_name).where("service_name like ?", "%#{params[:service_id]}" )   
    @service_list = @services_inventories.map { |a|[a.service_name+" ",a.id]  } 



    # AssignedRequest
   

 @b_records = BasicMedRecord.find_by(patient_id: patient_id)

      if (defined?(@b_records)).nil? 

    @patient_id = @b_records.patient_id
     patient_id = @patient_id
     
      @basic_med_id = @b_records.id
     basic_med_id = @basic_med_id

   else
   end

    respond_to do |format|

        format.html
        format.js
  end     
  end

  def view_med

     @assign_patient = AssignPatient.new
    #@clinic_document = ClinicDocument.new
    patient_id=params[:id] 

      @b_records = BasicMedRecord.find_by(patient_id: patient_id)
 
     @document1 = MedicationRequest.find_by(patient_id: patient_id)
      logger.info "document1: #{@document1.medication}"

    

    respond_to do |format|

        format.html
        format.js
  end

  end


  # PATCH/PUT /medication_requests/1
  # PATCH/PUT /medication_requests/1.json
  def update
    respond_to do |format|
      if @medication_request.update(medication_request_params)
        format.html { redirect_to @medication_request, notice: 'Medication request was successfully updated.' }
        format.json { render :show, status: :ok, location: @medication_request }
      else
        format.html { render :edit }
        format.json { render json: @medication_request.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /medication_requests/1
  # DELETE /medication_requests/1.json
  def destroy
    @medication_request.destroy
    respond_to do |format|
      format.html { redirect_to medication_requests_url, notice: 'Medication request was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_medication_request
      @medication_request = MedicationRequest.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def medication_request_params
      params.require(:medication_request).permit(:basic_medical_record_id, :patient_id, :user_id, :personnel_id, :status, :medication, :service_id)
    end
end
