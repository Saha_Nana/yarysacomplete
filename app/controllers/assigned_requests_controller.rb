class AssignedRequestsController < ApplicationController
  before_action :set_assigned_request, only: [:show, :edit, :update, :destroy]

  # GET /assigned_requests
  # GET /assigned_requests.json
  def retrive
    @store_items_save = []
    @store_requests = StoreRequest.all
    @store_inventory_masters = StoreInventoryMaster.all
    @store_inventory_categories = StoreInventoryCategory.all
    @assigned_request = AssignedRequest.all


    # store_items_save is an array to save the id from store_items
    logger.info "--------START-----------------"

    @manager_value = 12


    @multiple_requests_id =  params[:store_request][:id]

    #  logger.info ("Store Req Id --- #{@multiple_requests_id}")

    @multiple_requests_id.each do | value |

      param_obj = StoreRequest.find(value)      
      logger.info ("Param_obj --- #{param_obj.inspect}")
      
      user_now = param_obj.user_id
      store_inventory_categories_id_value  =  param_obj.store_inventory_categories_id
      store_inventory_master_id_value = param_obj.store_inventory_master_id
      quantity_value = param_obj.quantity
      comments_val = param_obj.comments
      date_assigned_value = Time.now.strftime("%y/%m/%d %H:%M:%S")
      date_requested_value = param_obj.date_requested
      status_val = param_obj.status
      store_req_id_value = param_obj.id
      request_user_role_id = param_obj.role_id

      #Dynamically picking manager Id from current User
      store_manager_id_value = current_user.id

      logger.info "Current user obj --- #{current_user}"


      logger.info "Store_Item_Id ---- #{store_inventory_master_id_value}"

      @save_params = AssignedRequest.new(user_id: user_now, store_inventory_categories_id: store_inventory_categories_id_value, store_inventory_master_id: store_inventory_master_id_value,
        quantity: quantity_value, comments: comments_val, status: status_val, date_assigned: date_assigned_value,
        store_request_id: store_req_id_value, store_manager_id: store_manager_id_value, assigned_to_role_id: request_user_role_id)

        update_status = StoreRequest.update(value, :status => 0 )

        logger.info ("status chnaged --- #{update_status}")


      respond_to do |format|
        if @save_params.save!
          format.html {   }
          format.json { render :show, status: :created, location: @store_request }
        else
          format.html { render :new }
          format.json { render json: @store_request.errors, status: :unprocessable_entity }
        end
      end


     #calling inventory master table by id
     @quanty = StoreInventoryMaster.find(store_inventory_master_id_value)

     logger.info ("Store Id details for assigned item -- #{@quanty.inspect}")

     logger.info ("assigned quantity --- #{quantity_value}")

     #quantity of item in inventory master
     @item_quant = @quanty.quantity

     logger.info ("Store Inventory quantity for assigned item -- #{@item_quant}")


     #Performing subtraction for items assigned
     @final_quanty =  @item_quant.to_i - quantity_value.to_i

     logger.info ("Quantity after subtraction --- #{@final_quanty}")

      #UPDATING DATABSE WITH NEW QUANTITY VALUE
     @answer = StoreInventoryMaster.where(id: store_inventory_master_id_value).update(:quantity => @final_quanty )

     logger.info ("wHAT WE GET --- #{@answer}")

    end
    redirect_to assigned_request_page_path, notice:'Item was successfully assigned.'
  end



  def confirm_dispense
    @new_dispensary = Dispensary.new

    @checked_value = params[:assigned_request][:id]
    @store_items_save = []
    @checked_value.each do |values|
      @store_items = AssignedRequest.joins("INNER JOIN store_inventory_masters ON store_inventory_masters.id = assigned_requests.store_inventory_master_id
        INNER JOIN store_inventory_categories ON store_inventory_categories.id = assigned_requests.store_inventory_categories_id").select("
        assigned_requests.id, assigned_requests.quantity as assigned_quantity, assigned_requests.comments as assigned_comments, assigned_requests.store_manager_id as inventory_user_id, assigned_requests.user_id as assigning_user,
        assigned_requests.status as assigned_status, assigned_requests.store_inventory_master_id as item_id, assigned_requests.store_inventory_categories_id as assigned_category_id,
        store_inventory_masters.item_name as assigned_item_name, store_inventory_masters.cost_price as assigned_item_cost_price,
        store_inventory_masters.selling_price as assigned_item_selling_price, store_inventory_masters.manufaturer_name as assigned_item_manufacturer,
        store_inventory_masters.id as store_inventory_item_id, store_inventory_categories.name as category_name").find(values)

      logger.info "@store_items ---#{@store_items.inspect}"
      logger.info "@category_name ---#{@store_items.category_name}"
      logger.info "@assigned_item_name ---#{@store_items.assigned_item_name}"

      @store_items_save << @store_items

        logger.info "@store_items_save ---#{@store_items_save.inspect}"

    end

    respond_to do |format|
      format.js
      format.html
    end

  end


  def index
    # @assigned_requests = AssignedRequest.all

    @store_inventory_categories = StoreInventoryCategory.all

    @store_inventory_masters = StoreInventoryMaster.all

    @assigned_requests = AssignedRequest.joins("INNER JOIN store_requests ON store_requests.id = assigned_requests.store_request_id INNER JOIN store_inventory_categories ON store_inventory_categories.id = assigned_requests.store_inventory_categories_id INNER JOIN store_inventory_masters ON store_inventory_masters.id = assigned_requests.store_inventory_master_id ")
    .select("assigned_requests.id, store_requests.user_id as requesting_user, assigned_requests.user_id as assigned_to_user, store_request_id, store_requests.quantity as requested_quantity, assigned_requests.comments as assigning_comments, store_inventory_masters.quantity as store_quantity_available, store_inventory_categories.name as category_name, item_name, cost_price, selling_price, manufaturer_name,item_location, date_requested, assigned_requests.store_inventory_master_id as store_invent_master,
             store_requests.status as request_status, store_requests.store_manager_id as request_to_store_manager, assigned_requests.store_manager_id as assigned_by_store_manager, assigned_requests.date_assigned as assigned_date, store_requests.date_assigned as store_request_assigned_date , assigned_requests.status as assigned_status, assigned_requests.quantity as assigned_quantity, assigned_requests.store_inventory_categories_id as store_invent_cat")
             .paginate(:page=>params[:page],per_page:3).order('id desc')
  end

  # GET /assigned_requests/1
  # GET /assigned_requests/1.json
  def show
  end

  # GET /assigned_requests/new
  def new
    @assigned_request = AssignedRequest.new
    @store_inventory_masters = StoreInventoryMaster.all
    @store_requests = StoreRequest.all
    @assigned_requests = AssignedRequest.joins("INNER JOIN store_requests ON store_requests.id = assigned_requests.store_request_id INNER JOIN store_inventory_categories ON store_inventory_categories.id = assigned_requests.store_inventory_categories_id INNER JOIN store_inventory_masters ON store_inventory_masters.id = assigned_requests.store_inventory_master_id ")
      .select("assigned_requests.id, store_inventory_categories.id, store_inventory_categories_id, store_requests.user_id as requesting_user, store_request_id, store_requests.quantity as requested_quantity, assigned_requests.comments,store_inventory_masters.quantity as store_quantity_available, store_inventory_categories.name as category_name, item_name, cost_price,
      selling_price, manufaturer_name,item_location, date_requested,assigned_requests.status as assigned_status,store_requests.status as request_status, store_manager_id, date_assigned, assigned_requests.status as assigned_status, assigned_requests.quantity as assigned_quantity,store_inventory_master_id").paginate(:page=>params[:page],per_page:5).order('id desc')

    @dispensary = Dispensary.new
     #Getting store request id
    @store_req_id = params[:id]

    #Assigned_request quantity
    @assigned_quantity = params[:quantity]

    @name_of_item = params[:item_name]

    # getting id of item
    @store_inventory_master = params[:store_inventory_master_id]

    #User id params
    @user = params[:user_id]

    #@store_quant_avail = params[:store_inventory_masters][:store_inventory_masters.quantity]

    #getting category id
    @category_id= params[:store_inventory_categories_id]

    #getting category id
    @category_name = params[:category_name]

  end

  # GET /assigned_requests/1/edit
  def edit
    @store_req_id = params[:id]
    @name_of_item = params[:item_name]
    @store_inventory_master_id = params[:store_inventory_master_id]
    @store_inventory_categories = params[:store_inventory_categories_id]
  end


  # POST /assigned_requests
  # POST /assigned_requests.json
  def create
    @store_inventory_masters = StoreInventoryMaster.all
    @store_requests = StoreRequest.all

    @assigned_request = AssignedRequest.new(assigned_request_params)

    # logger.info "params check -- #{@assigned_request.inspect}"

    respond_to do |format|
      if @assigned_request.save
        format.html { redirect_to assigned_request_page_path, notice: 'Item was successfully assigned.'  }
        format.json { render :show, status: :created, location: @store_request }
      else
       format.html { render :new }
         format.json { render json: @store_request.errors, status: :unprocessable_entity }
      end
    end

    @store_Item_master_Id = params[:assigned_request][:store_inventory_master_id] # getting id of item
    @assigned_quantity = params[:assigned_request][:quantity]

    #calling inventory master table by id
    @quanty = StoreInventoryMaster.find(@store_Item_master_Id)

    logger.info ("Store Id details for assigned item -- #{@quanty.inspect}")

    logger.info ("assigned quantity --- #{@assigned_quantity}")

    #quantity of item in inventory master

    @item_quant = @quanty.quantity

    logger.info ("Store Inventory quantity for assigned item -- #{@item_quant}")


    #Performing subtraction for items assigned

    @final_quanty =  @item_quant.to_i - @assigned_quantity.to_i

    logger.info ("Quantity after subtraction --- #{@final_quanty}")

     #UPDATING DATABSE WITH NEW QUANTITY VALUE
    @answer = StoreInventoryMaster.where(id: @store_Item_master_Id).update(:quantity => @final_quanty )

    logger.info ("wHAT WE GET --- #{@answer}")

  end

  # PATCH/PUT /assigned_requests/1
  # PATCH/PUT /assigned_requests/1.json
  def update

    @store_req_id = params[:id]
    @name_of_item = params[:item_name]
    @store_inventory_master_id = params[:store_inventory_master_id]

    respond_to do |format|
      if @assigned_request.update(assigned_request_params)
        format.html { redirect_to assigned_requests_path, notice: 'Item updated successfully.' }
        format.json { render :show, status: :ok, location: @assigned_request }
      else
        format.html { render :edit }
        format.json { render json: @assigned_request.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /assigned_requests/1
  # DELETE /assigned_requests/1.json
  def destroy
    @assigned_request.destroy
    respond_to do |format|
      format.html { redirect_to assigned_requests_url, notice: 'Item successfully deleted.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_assigned_request
      @assigned_request = AssignedRequest.find(params[:id])
      # @assigned_request = AssignedRequest.find(params[:item_name])
    end

    # Only allow a list of trusted parameters through.
    def assigned_request_params
      params.require(:assigned_request).permit(:id, :store_request_id, :store_inventory_master_id, :store_inventory_categories_id, :quantity, :user_id, :date_assigned, :comments, :status, :store_manager_id, :assigned_to_role_id)
    end
end
