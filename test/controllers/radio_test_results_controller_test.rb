require 'test_helper'

class RadioTestResultsControllerTest < ActionController::TestCase
  setup do
    @radio_test_result = radio_test_results(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:radio_test_results)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create radio_test_result" do
    assert_difference('RadioTestResult.count') do
      post :create, radio_test_result: { results: @radio_test_result.results }
    end

    assert_redirected_to radio_test_result_path(assigns(:radio_test_result))
  end

  test "should show radio_test_result" do
    get :show, id: @radio_test_result
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @radio_test_result
    assert_response :success
  end

  test "should update radio_test_result" do
    patch :update, id: @radio_test_result, radio_test_result: { results: @radio_test_result.results }
    assert_redirected_to radio_test_result_path(assigns(:radio_test_result))
  end

  test "should destroy radio_test_result" do
    assert_difference('RadioTestResult.count', -1) do
      delete :destroy, id: @radio_test_result
    end

    assert_redirected_to radio_test_results_path
  end
end
