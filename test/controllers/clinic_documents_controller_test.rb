require 'test_helper'

class ClinicDocumentsControllerTest < ActionController::TestCase
  setup do
    @clinic_document = clinic_documents(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:clinic_documents)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create clinic_document" do
    assert_difference('ClinicDocument.count') do
      post :create, clinic_document: { basic_medical_record_id: @clinic_document.basic_medical_record_id, clinic_exams: @clinic_document.clinic_exams, complaints: @clinic_document.complaints, final_diagnosis: @clinic_document.final_diagnosis, investigations: @clinic_document.investigations, patient_id: @clinic_document.patient_id, status: @clinic_document.status, symptoms: @clinic_document.symptoms, treatment_plans: @clinic_document.treatment_plans, user_id: @clinic_document.user_id, vital_signs: @clinic_document.vital_signs }
    end

    assert_redirected_to clinic_document_path(assigns(:clinic_document))
  end

  test "should show clinic_document" do
    get :show, id: @clinic_document
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @clinic_document
    assert_response :success
  end

  test "should update clinic_document" do
    patch :update, id: @clinic_document, clinic_document: { basic_medical_record_id: @clinic_document.basic_medical_record_id, clinic_exams: @clinic_document.clinic_exams, complaints: @clinic_document.complaints, final_diagnosis: @clinic_document.final_diagnosis, investigations: @clinic_document.investigations, patient_id: @clinic_document.patient_id, status: @clinic_document.status, symptoms: @clinic_document.symptoms, treatment_plans: @clinic_document.treatment_plans, user_id: @clinic_document.user_id, vital_signs: @clinic_document.vital_signs }
    assert_redirected_to clinic_document_path(assigns(:clinic_document))
  end

  test "should destroy clinic_document" do
    assert_difference('ClinicDocument.count', -1) do
      delete :destroy, id: @clinic_document
    end

    assert_redirected_to clinic_documents_path
  end
end
