require 'test_helper'

class ServicesInventoriesControllerTest < ActionController::TestCase
  setup do
    @services_inventory = services_inventories(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:services_inventories)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create services_inventory" do
    assert_difference('ServicesInventory.count') do
      post :create, services_inventory: { category_id: @services_inventory.category_id, price: @services_inventory.price, quantifiable: @services_inventory.quantifiable, service_name: @services_inventory.service_name }
    end

    assert_redirected_to services_inventory_path(assigns(:services_inventory))
  end

  test "should show services_inventory" do
    get :show, id: @services_inventory
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @services_inventory
    assert_response :success
  end

  test "should update services_inventory" do
    patch :update, id: @services_inventory, services_inventory: { category_id: @services_inventory.category_id, price: @services_inventory.price, quantifiable: @services_inventory.quantifiable, service_name: @services_inventory.service_name }
    assert_redirected_to services_inventory_path(assigns(:services_inventory))
  end

  test "should destroy services_inventory" do
    assert_difference('ServicesInventory.count', -1) do
      delete :destroy, id: @services_inventory
    end

    assert_redirected_to services_inventories_path
  end
end
