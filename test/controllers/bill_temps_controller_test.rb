require 'test_helper'

class BillTempsControllerTest < ActionController::TestCase
  setup do
    @bill_temp = bill_temps(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:bill_temps)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create bill_temp" do
    assert_difference('BillTemp.count') do
      post :create, bill_temp: { patient_id: @bill_temp.patient_id, status: @bill_temp.status, user_id: @bill_temp.user_id }
    end

    assert_redirected_to bill_temp_path(assigns(:bill_temp))
  end

  test "should show bill_temp" do
    get :show, id: @bill_temp
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @bill_temp
    assert_response :success
  end

  test "should update bill_temp" do
    patch :update, id: @bill_temp, bill_temp: { patient_id: @bill_temp.patient_id, status: @bill_temp.status, user_id: @bill_temp.user_id }
    assert_redirected_to bill_temp_path(assigns(:bill_temp))
  end

  test "should destroy bill_temp" do
    assert_difference('BillTemp.count', -1) do
      delete :destroy, id: @bill_temp
    end

    assert_redirected_to bill_temps_path
  end
end
