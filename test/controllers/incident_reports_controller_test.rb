require 'test_helper'

class IncidentReportsControllerTest < ActionController::TestCase
  setup do
    @incident_report = incident_reports(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:incident_reports)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create incident_report" do
    assert_difference('IncidentReport.count') do
      post :create, incident_report: { action_taken: @incident_report.action_taken, incidate_date: @incident_report.incidate_date, incident_description: @incident_report.incident_description, incident_type: @incident_report.incident_type, status: @incident_report.status }
    end

    assert_redirected_to incident_report_path(assigns(:incident_report))
  end

  test "should show incident_report" do
    get :show, id: @incident_report
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @incident_report
    assert_response :success
  end

  test "should update incident_report" do
    patch :update, id: @incident_report, incident_report: { action_taken: @incident_report.action_taken, incidate_date: @incident_report.incidate_date, incident_description: @incident_report.incident_description, incident_type: @incident_report.incident_type, status: @incident_report.status }
    assert_redirected_to incident_report_path(assigns(:incident_report))
  end

  test "should destroy incident_report" do
    assert_difference('IncidentReport.count', -1) do
      delete :destroy, id: @incident_report
    end

    assert_redirected_to incident_reports_path
  end
end
