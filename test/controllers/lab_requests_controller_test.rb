require 'test_helper'

class LabRequestsControllerTest < ActionController::TestCase
  setup do
    @lab_request = lab_requests(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:lab_requests)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create lab_request" do
    assert_difference('LabRequest.count') do
      post :create, lab_request: { basic_medical_record_id: @lab_request.basic_medical_record_id, lab_type: @lab_request.lab_type, patient_id: @lab_request.patient_id, personnel_id: @lab_request.personnel_id, status: @lab_request.status, user_id: @lab_request.user_id }
    end

    assert_redirected_to lab_request_path(assigns(:lab_request))
  end

  test "should show lab_request" do
    get :show, id: @lab_request
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @lab_request
    assert_response :success
  end

  test "should update lab_request" do
    patch :update, id: @lab_request, lab_request: { basic_medical_record_id: @lab_request.basic_medical_record_id, lab_type: @lab_request.lab_type, patient_id: @lab_request.patient_id, personnel_id: @lab_request.personnel_id, status: @lab_request.status, user_id: @lab_request.user_id }
    assert_redirected_to lab_request_path(assigns(:lab_request))
  end

  test "should destroy lab_request" do
    assert_difference('LabRequest.count', -1) do
      delete :destroy, id: @lab_request
    end

    assert_redirected_to lab_requests_path
  end
end
