# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_02_02_092630) do

  create_table "active_storage_attachments", options: "ENGINE=InnoDB DEFAULT CHARSET=latin1", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", options: "ENGINE=InnoDB DEFAULT CHARSET=latin1", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "admission_review_notes", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1", force: :cascade do |t|
    t.integer "patient_id"
    t.integer "user_id"
    t.integer "clinic_document_id"
    t.text "review_text"
    t.boolean "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "admissions", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "patient_id"
    t.string "admission_note"
    t.integer "user_id"
    t.integer "personnel_id"
    t.boolean "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "room_no"
    t.text "discharge_note"
    t.integer "admission_days"
  end

  create_table "appointments", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "description"
    t.datetime "app_time"
    t.boolean "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "doctor_id"
    t.string "color"
    t.string "patient_id"
  end

  create_table "assign_patients", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "patient_id"
    t.integer "role_id"
    t.boolean "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "personnel_id"
    t.string "user_id"
    t.integer "basic_medical_record_id"
    t.string "service_id"
  end

  create_table "assigned_requests", options: "ENGINE=InnoDB DEFAULT CHARSET=latin1", force: :cascade do |t|
    t.integer "store_inventory_master_id"
    t.integer "store_inventory_categories_id"
    t.integer "quantity"
    t.integer "user_id"
    t.datetime "date_assigned"
    t.text "comments"
    t.boolean "status"
    t.integer "store_manager_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "store_request_id"
    t.integer "assigned_to_role_id"
  end

  create_table "basic_med_records", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "patient_id"
    t.string "blood_pressure"
    t.string "pulse"
    t.string "temp"
    t.string "respiratory_rate"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "status"
    t.text "complaints"
    t.boolean "admission_status"
    t.string "saturation"
    t.string "weight"
    t.string "height"
  end

  create_table "bill_histories", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "service_id"
    t.integer "patient_id"
    t.float "price"
    t.boolean "status"
    t.integer "quantity"
    t.decimal "unit_price", precision: 10
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "lab_order_id"
    t.boolean "delete_status"
  end

  create_table "bill_temps", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "patient_id"
    t.integer "user_id"
    t.boolean "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "categories", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "category_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "clinic_documents", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "patient_id"
    t.integer "basic_medical_record_id"
    t.integer "user_id"
    t.boolean "status"
    t.text "complaints"
    t.text "symptoms"
    t.text "vital_signs"
    t.text "clinic_exams"
    t.text "final_diagnosis"
    t.text "investigations"
    t.text "treatment_plans"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "med_history"
    t.text "social_history"
    t.text "allergies"
    t.text "history_complaints"
    t.text "direct_question"
    t.text "systems_review"
  end

  create_table "dispensaries", options: "ENGINE=InnoDB DEFAULT CHARSET=latin1", force: :cascade do |t|
    t.integer "assigned_requests_id"
    t.boolean "status"
    t.text "comments"
    t.datetime "date_dispensed"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "store_inventory_master_id"
    t.integer "store_inventory_categories_id"
    t.text "dosage"
    t.integer "patient_id"
    t.string "service_id"
    t.integer "order_request_id"
    t.integer "quantity"
    t.integer "user_id"
  end

  create_table "fluids", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1", force: :cascade do |t|
    t.text "orals"
    t.integer "orals_amount"
    t.string "iv_fluids"
    t.integer "fluids_amount"
    t.string "urine_amount"
    t.string "emesis_amount"
    t.string "drainage_amount"
    t.integer "user_id"
    t.datetime "time"
    t.integer "patient_id"
    t.boolean "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "incident_reports", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1", force: :cascade do |t|
    t.text "incident_type"
    t.date "incidate_date"
    t.text "incident_description"
    t.text "action_taken"
    t.boolean "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "user_id"
  end

  create_table "lab_order_lists", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "lab_name"
    t.decimal "price", precision: 10
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "lab_requests", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "basic_medical_record_id"
    t.integer "patient_id"
    t.integer "user_id"
    t.integer "personnel_id"
    t.boolean "status"
    t.string "lab_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "service_id"
    t.string "lab_order_id"
    t.string "document_file_name"
    t.string "document_content_type"
    t.integer "document_file_size"
    t.datetime "document_updated_at"
    t.boolean "delete_status"
  end

  create_table "medication_requests", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "basic_medical_record_id"
    t.integer "patient_id"
    t.integer "user_id"
    t.integer "personnel_id"
    t.boolean "status"
    t.string "medication"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "service_id"
  end

  create_table "medication_temps", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "patient_id"
    t.string "service_id"
    t.boolean "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "models", options: "ENGINE=InnoDB DEFAULT CHARSET=latin1", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_models_on_email", unique: true
    t.index ["reset_password_token"], name: "index_models_on_reset_password_token", unique: true
  end

  create_table "nurse_notes", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1", force: :cascade do |t|
    t.text "notes"
    t.integer "user_id"
    t.datetime "time"
    t.integer "patient_id"
    t.boolean "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "nurse_procedures", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1", force: :cascade do |t|
    t.integer "service_id"
    t.integer "patient_id"
    t.boolean "status"
    t.text "complaints"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "user_id"
    t.integer "quantity"
  end

  create_table "order_masters", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "order_name"
    t.boolean "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "order_requests", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "patient_id"
    t.integer "user_id"
    t.integer "personnel_id"
    t.boolean "status"
    t.integer "order_master_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "service_id"
    t.integer "drug_quantity"
    t.text "dosage"
    t.boolean "delete_status"
  end

  create_table "patients", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "title"
    t.string "surname"
    t.string "other_names"
    t.string "mobile_number"
    t.string "occupation"
    t.string "address"
    t.boolean "status"
    t.integer "user_id"
    t.string "alt_number"
    t.date "dob"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "card_no"
    t.string "gender", limit: 10
    t.string "religion", limit: 25
    t.string "emergency_c_name"
    t.string "emergency_c_number"
    t.string "insurance_cash"
    t.string "refers"
    t.boolean "assigned_status"
    t.boolean "pay_status"
    t.string "email"
    t.string "emergency_c_location"
    t.string "emergency_c_email"
    t.string "marital_status"
  end

  create_table "purchase_receipts", options: "ENGINE=InnoDB DEFAULT CHARSET=latin1", force: :cascade do |t|
    t.integer "store_purchase_id"
    t.string "image"
    t.integer "user_id"
    t.boolean "status"
    t.text "comments"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "radio_test_results", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "patient_id"
    t.integer "user_id"
    t.boolean "status"
    t.string "results"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "lab_id"
    t.integer "service_id"
    t.string "document_file_name"
    t.string "document_content_type"
    t.integer "document_file_size"
    t.datetime "document_updated_at"
    t.integer "radio_id"
  end

  create_table "radiology_requests", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "basic_medical_record_id"
    t.integer "patient_id"
    t.integer "user_id"
    t.integer "personnel_id"
    t.boolean "status"
    t.string "radiology"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "service_id"
    t.string "document_file_name"
    t.string "document_content_type"
    t.integer "document_file_size"
    t.datetime "document_updated_at"
    t.boolean "delete_status"
  end

  create_table "roles", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "role_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "service_inventory_histories", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "service_id"
    t.decimal "price", precision: 10
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "services_inventories", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "service_name"
    t.float "price"
    t.integer "category_id"
    t.boolean "quantifiable"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.float "insurance_price"
    t.integer "temp_quantity"
  end

  create_table "store_inventory_categories", options: "ENGINE=InnoDB DEFAULT CHARSET=latin1", force: :cascade do |t|
    t.string "name"
    t.boolean "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "store_inventory_masters", options: "ENGINE=InnoDB DEFAULT CHARSET=latin1", force: :cascade do |t|
    t.integer "store_inventory_categories_id"
    t.string "item_name"
    t.float "cost_price"
    t.float "selling_price"
    t.string "manufaturer_name"
    t.integer "user_id"
    t.integer "quantity"
    t.string "item_location"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "temp_quantity"
    t.datetime "date_of_purchase"
    t.boolean "status"
    t.string "image"
    t.float "insurance_price"
  end

  create_table "store_purchases", options: "ENGINE=InnoDB DEFAULT CHARSET=latin1", force: :cascade do |t|
    t.integer "store_inventory_categories_id"
    t.string "item_name"
    t.float "cost_price"
    t.integer "quantity"
    t.datetime "date_of_purchase"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "store_requests", options: "ENGINE=InnoDB DEFAULT CHARSET=latin1", force: :cascade do |t|
    t.integer "user_id"
    t.integer "store_inventory_categories_id"
    t.integer "store_inventory_master_id"
    t.integer "quantity"
    t.text "comments"
    t.boolean "status"
    t.datetime "date_requested"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "store_manager_id"
    t.datetime "date_assigned"
    t.integer "role_id"
  end

  create_table "test_results", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "patient_id"
    t.integer "user_id"
    t.boolean "status"
    t.string "results"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "lab_id"
    t.integer "service_id"
    t.string "document_file_name"
    t.string "document_content_type"
    t.integer "document_file_size"
    t.datetime "document_updated_at"
  end

  create_table "treatment_sheets", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1", force: :cascade do |t|
    t.string "drugs"
    t.integer "user_id"
    t.datetime "time"
    t.boolean "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "patient_id"
  end

  create_table "users", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "username"
    t.string "fullname"
    t.string "mobile_number"
    t.integer "role_id"
    t.boolean "status"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
end
