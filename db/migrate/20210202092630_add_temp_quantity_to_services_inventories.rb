class AddTempQuantityToServicesInventories < ActiveRecord::Migration[5.2]
  def change
    add_column :services_inventories, :temp_quantity, :integer
  end
end
